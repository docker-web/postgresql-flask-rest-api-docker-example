# Test basic db setup and configuration
import pytest
import xunitparser
import requests
from conftest import option
from DBAPI import DBAPI

## Setup that needs to occur prior to running unit tests
def setup_module():
    # Configure database to talk to localhost
    pytest.global_variable_1 = DBAPI()

    # Build out tables, columns, and populate rows
    command = '''
    CREATE TABLE Persons (
        personId serial PRIMARY KEY,
        firstName VARCHAR (50) NOT NULL,
        lastName VARCHAR (50) NOT NULL
    );
    '''
    pytest.global_variable_1.cur.execute(command)

## Teardown that needs to occur after running unit tests
def teardown_module():
    # Delete the test table
    command = "DROP TABLE Persons;"
    pytest.global_variable_1.cur.execute(command)

    # Close the connection to the local database
    pytest.global_variable_1.cur.close()
    pytest.global_variable_1.conn.close()

# Operations that occur prior to every test function
def setup_function():
    pass

# Operations that occur after every test function
def teardown_function():
    pass

# Validate the Postgres server version
def test_version():
    command = 'SELECT version()'
    assert pytest.global_variable_1.callDbWithStatement(command)[0].startswith("PostgreSQL 12.5")

# Direct write and read a row to the table
def test_write_row_direct():
    id = "123"
    first = "Ari"
    last = "Mahpour"

    # Write sample data into row
    command = "INSERT INTO Persons(personId, firstName, lastName) VALUES ('" + id + "', '" + first + "', '" + last + "')"
    pytest.global_variable_1.cur.execute(command)

    # Read back sample data
    command = "SELECT * FROM Persons WHERE personId='" + id + "'"
    response = pytest.global_variable_1.callDbWithStatement(command)
    assert str(response[0]) == id
    assert response[1] == first
    assert response[2] == last

# Test the Flask API server using requests
def test_api():
    id = "456"
    first = "John"
    last = "Doe"

    url = "http://localhost:5000/"
    post_api = "createPerson"
    get_api = "getPerson"
    homepage_text = "Flask test within Docker"

    # Basic homepage request test
    response = requests.get(url)
    assert response.text == homepage_text
    
    # Write to the database via POST
    personInfo = {"personId":id, "firstName":first, "lastName":last}
    response = requests.post(url + post_api, json=personInfo)
    assert response.status_code == 200

    # Read from the database via GET
    response = requests.get(url + get_api + "?personId=" + id)
    assert response.json()['firstName'] == first
    assert response.json()['lastName'] == last
    assert response.json()['personId'] == int(id)