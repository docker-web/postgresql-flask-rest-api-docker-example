def pytest_addoption(parser):
    """Add the --user option"""
    parser.addoption(
        '--db',
        action="store", default="local",
        help="Database type")

option = None

def pytest_configure(config):
    """Make cmdline arguments available to dbtest"""
    global option
    option = config.option