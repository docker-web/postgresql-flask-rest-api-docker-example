# Start the Flask app
python app.py &
FLASK_PID=$!
sleep 0.5

# Run the unit tests
python -m pytest -s test_dbtest.py --db "local" --junitxml=testLog.xml

# Stop the Flask app
kill $FLASK_PID