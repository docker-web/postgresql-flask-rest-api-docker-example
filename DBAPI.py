import os
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Database API to interact with local PostgreSQL or AWS RDS
class DBAPI:
    def __init__(self):
        # Configure database connection
        self.user = os.environ['POSTGRES_USER']
        self.password = os.environ['POSTGRES_PASSWORD']
        self.dbname = os.environ['POSTGRES_DB']
        self.host = os.environ['POSTGRES_HOST']
        
        print('Connecting to the PostgreSQL database...')
        self.conn = psycopg2.connect(host=self.host, database=self.dbname, user=self.user, password=self.password)
        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT);
        # create a cursor
        self.cur = self.conn.cursor()

    def callDbWithStatement(self, statement):
        # Debug printout
        print("Making Call: " + statement)

        # Run SQL command
        self.cur.execute(statement)
        return self.cur.fetchone()

# Main function to test the API
if __name__ == '__main__':
    db_conn = DBAPI()
    command = '''
    CREATE TABLE Persons (
        personId serial PRIMARY KEY,
        firstName VARCHAR (50) NOT NULL,
        lastName VARCHAR (50) NOT NULL
    );
    '''
    db_conn.cur.execute(command)