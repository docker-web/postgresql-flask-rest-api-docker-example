from flask import Flask, request, jsonify
from DBAPI import DBAPI

# Call the Flask and DB Connection constructors
app = Flask(__name__)
local_db = DBAPI()

@app.route('/getPerson') # API 1 - getPerson
def getPerson():
    personId = request.args.get('personId')
    response = local_db.callDbWithStatement("SELECT * FROM Persons WHERE personId='" + str(personId) + "'" )
    person = {}

    # Response type for local DB will be a tuple
    if type(response) is tuple:
        person['personId'] = response[0]
        person['firstName'] = response[1]
        person['lastName'] = response[2]
    else:
        records = response['records']
        for record in records:
            person['personId'] = record[0]['longValue']
            person['firstName'] = record[1]['stringValue']
            person['lastName'] = record[2]['stringValue']
    return jsonify(person)

@app.route('/createPerson',  methods=['POST']) # API 2 - createPerson
def createPerson():
    request_data = request.get_json()
    personId = str(request_data['personId'])
    firstName = request_data['firstName']
    lastName = request_data['lastName']
    command = "INSERT INTO Persons(personId, firstName, lastName) VALUES ('" + personId + "', '" + firstName + "', '" + lastName + "');"
    local_db.cur.execute(command)
    return ""

@app.route('/')
def hello_world():
    return 'Flask test within Docker'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')