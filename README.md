# PostgreSQL Flask REST API Docker Example
Flask web application for RESTful API to interact with PostgreSQL within a Docker container.

## Automated test
Automated test has been created to validate the design. Use the Dockerfile-Test container for a self-hosted PostgreSQL

## Notes
This code has been adapted from [beabetterdevv/RDSServerlessFlask](https://github.com/beabetterdevv/RDSServerlessFlask).
